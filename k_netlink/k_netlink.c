
/*******************************
file:           k_netlink.c
description:    netlink_test
kernel:         linux-5.4.18
author:         zhubing
email:          zhubing@kylinos.cn
*******************************/
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/sched.h>
#include <net/sock.h>
#include <net/netlink.h>
#include <linux/netlink.h>


#define NETLINK_USER 22
/* 定义netlink协议类型，最大32 */
#define USER_MSG (NETLINK_USER + 1)

#define MSG_LEN 125

static DEFINE_MUTEX(nl_mutex);  /*多线程互斥， 必需，有可能多个用户进程同时向本模块发消息*/

static void nl_lock(void)
{
    mutex_lock(&nl_mutex);
}
static void nl_unlock(void)
{
    mutex_unlock(&nl_mutex);
}


static void test_netlink_rcv(struct sk_buff *skb);  /* 回调函数 */

/* netlink 内核配置参数 */
struct netlink_kernel_cfg cfg = {
    //unsigned int    groups;
    //unsigned int    flags;
    //struct mutex    *cb_mutex;
    //void        (*bind)(int group);
    //bool        (*compare)(struct net *net, struct sock *sk);
    .input = test_netlink_rcv,/* input 回调函数 */
};

static struct sock *netlink_sockfd = NULL;   /*模块内部全局的netlink套接字*/


/*
    功能：发送消息给用户空间客户端
    参数：@msg:消息字符串
         @pod:客户端的进程id
*/
int send_msg(char *msg,  int u_pid)
{
    struct sk_buff *nl_skb;
    struct nlmsghdr *nlh;   /* 定义消息体 */

    int se=0;

    /* 创建sk_buff 空间 */
    nl_skb = nlmsg_new(strlen(msg), GFP_ATOMIC);
    if (!nl_skb) {
        printk("netlink_alloc_skb error\n");
        return -1;
    }
    /*将header填充到skb中*/
    nlh = nlmsg_put(nl_skb, 0, 0, USER_MSG, strlen(msg), 0);
    if (nlh == NULL) {
        printk("put error\n");
        nlmsg_free(nl_skb);
        return -1;
    }
    /*拷贝data*/
    memcpy(nlmsg_data(nlh), msg, strlen(msg));

    /* 发送单播消息 */
    se = netlink_unicast(netlink_sockfd, nl_skb, u_pid, MSG_DONTWAIT);

    return se;
}

static void test_netlink_rcv(struct sk_buff *skb)
{
    struct nlmsghdr *nlh = NULL;    /* 定义消息体 */

    char *data = NULL;
    char msg[MSG_LEN]={0};

    nl_lock();  /* 上锁 */
    printk("skb->len %u\n", skb->len);
    if (skb->len >= nlmsg_total_size(0))
    {
        nlh = nlmsg_hdr(skb);   /* 获取到netlink报头 */
        data = NLMSG_DATA(nlh); /* 宏NLMSG_DATA(nlh)用于取得消息的数据部分的首地址 */

        if (data) {
            printk("kernel receive data : %s\n", data);
            strcpy(msg,"hello,nice to meet you");
            send_msg(msg,nlh->nlmsg_pid);           /* 回复消息 */
            printk("kernel send data : %s\n", msg);
       }
    }
    nl_unlock(); /* 解锁 */
}


static int __init test_netlink_init(void)
{
    printk("test netlink init\n");

    netlink_sockfd = netlink_kernel_create(&init_net, USER_MSG, &cfg);   /* 创建内核 socket */
    if (netlink_sockfd == NULL) {
        printk("test netlink init error\n");
        return -1;
    }

    printk("test netlink init ok\n");

    return 0;
}
static void __exit test_netlink_exit(void)
{
    netlink_kernel_release(netlink_sockfd);  /* 释放套接字 */
    netlink_sockfd = NULL;

    printk("test netlink exit\n");
}

module_init(test_netlink_init);
module_exit(test_netlink_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("zhubing");
MODULE_DESCRIPTION("netlink_test");
