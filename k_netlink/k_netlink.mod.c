#include <linux/build-salt.h>
#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

BUILD_SALT;

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__section(.gnu.linkonce.this_module) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used __section(__versions) = {
	{ 0xf791d5f9, "module_layout" },
	{ 0x209649b7, "netlink_kernel_release" },
	{ 0x5758ca2e, "__netlink_kernel_create" },
	{ 0x70e8c3e2, "init_net" },
	{ 0xdecd0b29, "__stack_chk_fail" },
	{ 0x409bcb62, "mutex_unlock" },
	{ 0x2ab7989d, "mutex_lock" },
	{ 0x6938dfc7, "kfree_skb" },
	{ 0xc5850110, "printk" },
	{ 0x24d196a6, "netlink_unicast" },
	{ 0x69acdf38, "memcpy" },
	{ 0x838b87cd, "__nlmsg_put" },
	{ 0xa33be184, "__alloc_skb" },
	{ 0x754d539c, "strlen" },
	{ 0xbdfb6dbb, "__fentry__" },
};

MODULE_INFO(depends, "");


MODULE_INFO(srcversion, "0ADBC45E6E040B7D2CC6473");
