#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MSG_LEN 125


#define MY_PERM_READ 1
#define MY_PERM_WRITE 2
#define MY_PERM_EXEC 3
#define MY_PERM_RENAME 4
#define MY_PERM_DELETE 5
#define MY_PERM_CHMOD 6

int lsm_handle_data(char *data,char *filename)
{
    int i=0;
    while(data[i]!=' ')
    {
        filename[i]=data[i];
        i++;
    }
    i++;

    switch(data[i])
    {
        case '1':
            return MY_PERM_READ;
        case '2':
            return MY_PERM_WRITE;
        case '3':
            return MY_PERM_EXEC;
        case '4':
            return MY_PERM_RENAME;
        case '5':
            return MY_PERM_DELETE;
        case '6':
            return MY_PERM_CHMOD;
        default:
            return -1;
    }

}

int main()
{
    char data[125]={0};
    printf("%s\n",data);
    printf("%d\n",strlen(data));
    if(data[0] == 0)
        printf("1\n");
    char file[125]={0};
    /*
    int perm=lsm_handle_data(data,file);
    printf("file :%s\n",file);
    printf("perm:%d\n",perm);
    */
    return 0;


}
