#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MSG_LEN 125

char* handle_data(int argc,char **argv)
{
    printf("%d\n",argc);
    if(argc < 3 || argc >4)
        return NULL;
    int i;
    int data_size=0;
    for(i=1; i<argc ;i++)
    {
        data_size += strlen(argv[i]);
        data_size++;
    }

    if(data_size > MSG_LEN)
        return NULL;

    char *tmp=argv[2];
    if(tmp[0] != '-')
        return NULL;

    if( !strcmp(tmp,"r") || !strcmp(tmp,"w") || !strcmp(tmp,"exec") || !strcmp(tmp,"rename") || !strcmp(tmp,"delete") || !strcmp(tmp,"chmod"))
    {

        data=(char *)malloc(data_size);
        strcpy(data,argv[1]);
        *(data+strlen(argv[1]))=' ';
        if( !strcmp(tmp,"r"))
            *(data+strlen(argv[1])+1)='1';
        else if( !strcmp(tmp,"w"))
            *(data+strlen(argv[1])+1)='2';
        else if( !strcmp(tmp,"exec"))
            *(data+strlen(argv[1])+1)='3';
        else if( !strcmp(tmp,"rename"))
            *(data+strlen(argv[1])+1)='4';
        else if( !strcmp(tmp,"delete"))
            *(data+strlen(argv[1])+1)='5';
        else
             *(data+strlen(argv[1])+1)='6';
        return data;
    }
    return NULL;
}



int main(int argc ,char** argv)
{
    char *data=handle_data(argc,argv);
    if(data ==NULL)
        printf("参数错误!!!\n");
    printf("data:%s\n",data);
    return 0;
}
