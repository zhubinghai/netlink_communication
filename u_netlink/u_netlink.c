/*******************************
file:           u_netlink.c
description:    netlink_test
author:         zhubing
email:          zhubing@kylinos.cn
*******************************/
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <string.h>
#include <linux/netlink.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>


#define NETLINK_USER 22
/* 定义netlink协议类型，最大32 */
#define USER_MSG (NETLINK_USER + 1)

#define MSG_LEN 125

#define MAX_PLOAD 125

struct _my_msg {
    struct nlmsghdr hdr;
    char data[MSG_LEN];
};

char* handle_data(int argc,char **argv)
{
    if(argc < 3 || argc >4)
        return NULL;
    int i;
    int data_size=0;
    for(i=1; i<argc ;i++)
    {
        data_size += strlen(argv[i]);
        data_size++;
    }

    if(data_size > MSG_LEN)
        return NULL;

    char *tmp=argv[2];
    if(tmp[0] != '-')
        return NULL;

    tmp=tmp+1;
    char *data=NULL;

    if( !strcmp(tmp,"r") || !strcmp(tmp,"w") || !strcmp(tmp,"exec") || !strcmp(tmp,"rename") || !strcmp(tmp,"delete") || !strcmp(tmp,"chmod"))
    {

        data=(char *)malloc(data_size);
        strcpy(data,argv[1]);
        *(data+strlen(argv[1]))=' ';
        if( !strcmp(tmp,"r"))
            *(data+strlen(argv[1])+1)='1';
        else if( !strcmp(tmp,"w"))
            *(data+strlen(argv[1])+1)='2';
        else if( !strcmp(tmp,"exec"))
            *(data+strlen(argv[1])+1)='3';
        else if( !strcmp(tmp,"rename"))
            *(data+strlen(argv[1])+1)='4';
        else if( !strcmp(tmp,"delete"))
            *(data+strlen(argv[1])+1)='5';
        else
             *(data+strlen(argv[1])+1)='6';
        return data;
    }
    return NULL;
}

int main(int argc ,char **argv)
{

    char *data=handle_data(argc,argv);
    printf("data:%s\n",data);
    if(data ==NULL)
    {
        printf("参数错误!!!\n");
        printf("./a.out [path_file] -[r][w][exec][rename][delete][chmod] \n");
        return 0;
    }


    socklen_t addr_len;
    struct sockaddr_nl src_addr, dest_addr; //定义neilink协议套接字地址

    int sockfd;
    struct nlmsghdr *nlh = NULL;    /* 定义消息体 */
    struct _my_msg info;    //定义接受消息结构体

    //创建socket
    sockfd = socket(AF_NETLINK, SOCK_RAW, USER_MSG);
    if (sockfd < 0 ) {
        perror("create socket error\n");
        return (-1);
    }

    /* 初始化客户端的neilink地址，源地址 */
    memset(&src_addr, 0, sizeof(src_addr));
    src_addr.nl_family =  AF_NETLINK;   /* neilink协议族 */
    src_addr.nl_pid = getpid();  /*此处的pid通常用进程ID，表示来自用户态的唯一进程*/
    src_addr.nl_groups = 0; /* 用以多播，不需要时置为0 */

    /*绑定*/
    if (bind(sockfd, (struct sockaddr *)&src_addr, sizeof(src_addr)) != 0) {
        perror("bind fail");
        close(sockfd);
        return (-1);
    }

    //初始化目的地址
    memset(&dest_addr, 0, sizeof(dest_addr));
    dest_addr.nl_family = AF_NETLINK;   /* neilink协议族 */
    dest_addr.nl_pid = 0;   /* to kernel */
    dest_addr.nl_groups = 0;

    /*填写data*/
    nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PLOAD));    /* 分配包含头部分的消息长度的空间给消息体 */
    memset(nlh, 0, sizeof(struct nlmsghdr));
    nlh->nlmsg_len = NLMSG_SPACE(MAX_PLOAD);    /* Length of message including header 包含头部分的消息长度 */
    nlh->nlmsg_flags = 0;   /* Additional flags 消息标记 */
    nlh->nlmsg_type = 0;    /* Message content  消息状态 */
    nlh->nlmsg_seq = 0;     /* Sequence number */
    nlh->nlmsg_pid = src_addr.nl_pid;   /* Sending process port ID */

    memcpy(NLMSG_DATA(nlh), data, strlen(data));    /* 宏NLMSG_DATA(nlh)用于取得消息的数据部分的首地址 */

 // 发送消息到内核
    int se = sendto(sockfd, nlh, nlh->nlmsg_len, 0, (struct sockaddr *)&dest_addr, sizeof(struct sockaddr_nl));
    if (se < 0) {
        perror("sendto error: ");
        close(sockfd);
        return (-1);
    }
    printf("send msg:%s\n",(char *)NLMSG_DATA(nlh));

    /*接受信息*/
    /*
    memset(&info, 0, sizeof(info));
    int re = recvfrom(sockfd, &info, sizeof(struct _my_msg), 0, (struct sockaddr *)&dest_addr, &addr_len);
    if (re <= 0) {
        perror("recv from kernel error: ");
        close(sockfd);
        return (-1);
    }
    printf("msg receive from kernel : %s\n", info.data);
    */
    free((void *)nlh);
    close(sockfd);

    free(data);

    return 0;
}
